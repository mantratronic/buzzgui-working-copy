﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using BuzzGUI.Interfaces;

namespace ParameterWindowTest
{
	class TestParameterGroup : IParameterGroup
	{
		public IMachine Machine { get; set; }
		public ParameterGroupType Type { get; set; }
		public ReadOnlyCollection<IParameter> Parameters { get; set; }
		public int TrackCount { get; set; }
	}
}
