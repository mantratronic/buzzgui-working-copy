﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using BuzzGUI.Interfaces;

namespace ParameterWindowTest
{
	class TestMachineDLL : IMachineDLL
	{
		public IBuzz Buzz { get; set; }
		public string Name { get { return "Jeskola Test Machine"; } }
		public IMachineInfo Info { get { return new TestMachineInfo(); } }

		public ReadOnlyCollection<string> Presets { get { return new List<string>().AsReadOnly(); } }

		public bool IsMissing { get { return false; } }
		public bool IsCrashed {	get { return false; } }
		public System.Windows.Media.ImageSource Skin { get { return null; }	}
		public System.Windows.Media.ImageSource SkinLED	{ get { return null; } }
		public System.Windows.Size SkinLEDSize { get { return new System.Windows.Size(0, 0); } }
		public System.Windows.Point SkinLEDPosition	{ get { return new System.Windows.Point(0, 0); } }
		public System.Windows.Media.Color TextColor	{ get { return System.Windows.Media.Colors.Black; } }
		public IMachineGUIFactory GUIFactory { get { return null; }	}

		#region INotifyPropertyChanged Members

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		#endregion
	}
}
