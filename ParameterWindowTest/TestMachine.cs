﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using BuzzGUI.Interfaces;

namespace ParameterWindowTest
{
	class TestMachine : IMachine
	{
		public TestMachine()
		{
			var pg = new List<TestParameterGroup>();

			pg.Add(new TestParameterGroup()	{ Machine = this, Type = ParameterGroupType.Input, TrackCount = 1, Parameters = new List<IParameter>().AsReadOnly() });
			pg.Add(new TestParameterGroup() { Machine = this, Type = ParameterGroupType.Global, TrackCount = 1 });
			pg.Add(new TestParameterGroup() { Machine = this, Type = ParameterGroupType.Track, TrackCount = 4 }); 

			pg[1].Parameters = new List<IParameter>()
			{
				new TestParameter()
				{
					Group = pg[1], IndexInGroup = 0,
					Type = ParameterType.Byte,
					Name = "Global 1", Description = "global parameter 1",
					MinValue = 0, MaxValue = 127, NoValue = 255, DefValue = 0, Flags = ParameterFlags.State,
				}
			}.AsReadOnly();

			pg[2].Parameters = new List<IParameter>()
			{
				new TestParameter()
				{
					Group = pg[2], IndexInGroup = 0,
					Type = ParameterType.Byte,
					Name = "Track 1", Description = "track parameter 1",
					MinValue = 0, MaxValue = 127, NoValue = 255, DefValue = 0, Flags = ParameterFlags.State,
				}
			}.AsReadOnly();

			ParameterGroups = pg.Cast<IParameterGroup>().ToList().AsReadOnly();
		}

		public IMachineGraph Graph { get { return TestSong.TheSong; }	}
		public IMachineDLL DLL { get { return new TestMachineDLL(); } }

		public System.Collections.ObjectModel.ReadOnlyCollection<IMachineConnection> Inputs { get { return new List<IMachineConnection>().AsReadOnly(); } }
		public System.Collections.ObjectModel.ReadOnlyCollection<IMachineConnection> Outputs { get { return new List<IMachineConnection>().AsReadOnly(); } }

		public int InputChannelCount { get { return 1; } }
		public int OutputChannelCount { get { return 1; } }

		public string Name { get { return DLL.Info.ShortName; }	set	{ } }
		public Tuple<float, float> Position { get { return Tuple.Create(0.0f, 0.0f); } }
		public int OversampleFactor { get { return 1; } set { } }
		public int MIDIInputChannel { get { return 0; } set { } }

		public System.Collections.ObjectModel.ReadOnlyCollection<IParameterGroup> ParameterGroups { get; set; }
		public System.Collections.ObjectModel.ReadOnlyCollection<IAttribute> Attributes { get { return new List<IAttribute>().AsReadOnly(); } }
		public IMenuItem Commands { get { return null; } }
		public System.Collections.ObjectModel.ReadOnlyCollection<string> EnvelopeNames { get { return new List<string>().AsReadOnly(); } }
		public System.Collections.ObjectModel.ReadOnlyCollection<IPattern> Patterns { get { return new List<IPattern>().AsReadOnly(); } }

		public bool IsControlMachine { get { return false; } }
		public bool IsActive { get { return false; } }
		public bool IsMuted { get { return false; } set { } }
		public bool IsSoloed { get { return false; } set { } }
		public int LastEngineThread { get { return 0; } }
		public IMachineDLL PatternEditorDLL { get { return null; } }
		public int BaseOctave { get { return 4; } set {	} }

		public byte[] Data { get { return null; } set { } }

		public void ShowPresetEditor() { }
		public void CopyParameters() { }
		public void ShowHelp() { }
		public void UnbindAllMIDIControllers() { }
		public void ShowContextMenu(int x, int y) { }
		public void DoubleClick() { }
		public void ShowDialog(MachineDialog d, int x, int y) { }
		public void ExecuteCommand(int id) { }
		public string GetChannelName(bool input, int index) { return "channel"; }
		public byte[] SendGUIMessage(byte[] message) { return null; }
		public void SendControlChanges() { }
		public void SendMIDINote(int channel, int value, int velocity) { }

		#region INotifyPropertyChanged Members

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		#endregion
	}
}
