﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuzzGUI.Interfaces;

namespace ParameterWindowTest
{
	class TestParameter : IParameter
	{
		public IParameterGroup Group { get; set; }
		public int IndexInGroup { get; set; }
		public ParameterType Type { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int MinValue { get; set; }
		public int MaxValue { get; set; }
		public int NoValue { get; set; }
		public ParameterFlags Flags { get; set; }
		public int DefValue { get; set; }
		public string GetDisplayName(int track) { return Name; }

		int[] values = new int[256];

		public int GetValue(int track) { return values[track]; }
		public void SetValue(int track, int value) { values[track] = value; ValueChanged(); }

		public string DescribeValue(int value) { return value.ToString(); }

		public void BindToMIDIController(int track, int mcindex) { }

		public event Action ValueChanged;
		public event Action ValueDescriptionChanged;

		#region INotifyPropertyChanged Members

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		#endregion


	}
}
