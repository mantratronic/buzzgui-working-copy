﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuzzGUI.Interfaces;

namespace ParameterWindowTest
{
	class TestBuzz : IBuzz
	{
		static IBuzz theBuzz;
		public static IBuzz TheBuzz
		{
			get
			{
				if (theBuzz == null) theBuzz = new TestBuzz();
				return theBuzz;
			}
		}

		public ISong Song { get { return TestSong.TheSong; } }
		public BuzzView ActiveView { get { return BuzzView.MachineView; } set { } }
		public double MasterVolume { get { return 1.0; } set { } }
		public Tuple<double, double> VUMeterLevel { get { return Tuple.Create(0.0, 0.0); } }
		public int BPM { get { return 126; } set { } }
		public int TPB { get { return 4; } set { } }
		public int Speed { get { return 0; } set { } }
		public bool Playing	{ get { return false; } set { } }
		public bool Recording { get { return false; } set { } }
		public bool Looping { get { return false; } set	{ } }
		public bool AudioDeviceDisabled { get { return false; }	set { } }
		public System.Collections.ObjectModel.ReadOnlyCollection<string> MIDIControllers {	get { return new List<string>().AsReadOnly(); } }
		public IIndex<string, System.Windows.Media.Color> ThemeColors { get { throw new NotImplementedException(); } }
		public IMenuItem MachineIndex { get { throw new NotImplementedException(); } }
		public IMachine MIDIFocusMachine { get { return null; } set { } }
		public bool MIDIFocusLocked { get { return false; } set	{ } }
		public bool MIDIActivity { get { throw new NotImplementedException(); }	}
		public bool IsPianoKeyboardVisible { get { return false; } set { } }
		public int BuildNumber { get { return 12345; } }
		public void ExecuteCommand(BuzzCommand cmd)	{ }
		public bool CanExecuteCommand(BuzzCommand cmd) { return false; }

		public event Action<IOpenSong> OpenSong;
		public event Action<ISaveSong> SaveSong;

		public void DCWriteLine(string s) {	}
		public void ActivatePatternEditor() { }
		public void SetPatternEditorMachine(IMachine m)	{ }
		public void SetPatternEditorPattern(IPattern p) { }
		public void SendMIDIInput(int data) { }

		public event Action<int> MIDIInput;

		#region INotifyPropertyChanged Members

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		#endregion

		#region IBuzz Members


		public IDictionary<string, IMachineDLL> MachineDLLs
		{
			get { throw new NotImplementedException(); }
		}

		public System.Collections.ObjectModel.ReadOnlyCollection<IInstrument> Instruments
		{
			get { throw new NotImplementedException(); }
		}

		#endregion
	}
}
