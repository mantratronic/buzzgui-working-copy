﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BuzzGUI.ParameterWindow;

namespace ParameterWindowTest
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

			this.Loaded += (sender, e) =>
			{
				var vm = new ParameterWindowVM() { Machine = new TestMachine() };
				var pw = new ParameterWindow() { DataContext = vm, Owner = this };
				pw.Show();

				var pwg = new ParameterWindowGlass() { DataContext = vm, Owner = this };
				pwg.Show();

			};
		}
	}
}
