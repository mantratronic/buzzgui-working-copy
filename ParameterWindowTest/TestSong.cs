﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuzzGUI.Interfaces;

namespace ParameterWindowTest
{
	class TestSong : ISong
	{
		static ISong theSong;
		public static ISong TheSong
		{
			get
			{
				if (theSong == null) theSong = new TestSong();
				return theSong;
			}
		}

		public int PlayPosition	{ get { return 0; }	set { } }
		public int LoopStart { get { return 0; } set { } }
		public int LoopEnd { get { return 0; } set { } }
		public int SongEnd { get { return 0; } set { } }
		public System.Collections.ObjectModel.ReadOnlyCollection<ISequence> Sequences {	get { return new List<ISequence>().AsReadOnly(); } }

		public IWavetable Wavetable
		{
			get { throw new NotImplementedException(); }
		}

		public IDictionary<string, object> Associations
		{
			get { throw new NotImplementedException(); }
		}

		public event Action<int> SequenceAdded;
		public event Action<int> SequenceRemoved;
		public event Action<int> SequenceChanged;

		public void AddSequence(IMachine m)
		{
			throw new NotImplementedException();
		}

		public void RemoveSequence(ISequence s)
		{
			throw new NotImplementedException();
		}

		public void SwapSequences(ISequence s, ISequence t)
		{
			throw new NotImplementedException();
		}

		public IBuzz Buzz { get { return TestBuzz.TheBuzz; } }

		public System.Collections.ObjectModel.ReadOnlyCollection<IMachine> Machines
		{
			get { throw new NotImplementedException(); }
		}

		public IMachine CreateMachine(string dllname, string name, float x, float y)
		{
			throw new NotImplementedException();
		}

		public void CreateMachine(int id, float x, float y)
		{
			throw new NotImplementedException();
		}

		public void ReplaceMachine(IMachine m, int id, float x, float y)
		{
			throw new NotImplementedException();
		}

		public void InsertMachine(IMachineConnection m, int id, float x, float y)
		{
			throw new NotImplementedException();
		}

		public void CloneMachine(IMachine m, float x, float y)
		{
			throw new NotImplementedException();
		}

		public void MoveMachines(IEnumerable<Tuple<IMachine, Tuple<float, float>>> mm)
		{
			throw new NotImplementedException();
		}

		public void ConnectMachines(IMachine src, IMachine dst, int srcchn, int dstchn)
		{
			throw new NotImplementedException();
		}

		public void DisconnectMachines(IMachineConnection mc)
		{
			throw new NotImplementedException();
		}

		public void DeleteMachines(IEnumerable<IMachine> m)
		{
			throw new NotImplementedException();
		}

		public void SetConnectionParameter(IMachineConnection mc, int index, int oldvalue, int newvalue)
		{
			throw new NotImplementedException();
		}

		public void SetConnectionChannel(IMachineConnection mc, bool destination, int channel)
		{
			throw new NotImplementedException();
		}

		public bool CanConnectMachines(IMachine src, IMachine dst)
		{
			throw new NotImplementedException();
		}

		public void ShowContextMenu(int x, int y)
		{
			throw new NotImplementedException();
		}

		public void DoubleClick(int x, int y)
		{
			throw new NotImplementedException();
		}

		public void QuickNewMachine(char firstch)
		{
			throw new NotImplementedException();
		}

		public void ImportSong(float x, float y)
		{
			throw new NotImplementedException();
		}

		public event Action<IMachine> MachineAdded;

		public event Action<IMachine> MachineRemoved;

		public event Action<IMachineConnection> ConnectionAdded;

		public event Action<IMachineConnection> ConnectionRemoved;

		#region INotifyPropertyChanged Members

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		#endregion

		#region IMachineGraph Members


		void IMachineGraph.CreateMachine(string machine, string instrument, float x, float y)
		{
			throw new NotImplementedException();
		}

		public void ReplaceMachine(IMachine m, string machine, string instrument, float x, float y)
		{
			throw new NotImplementedException();
		}

		public void InsertMachine(IMachineConnection m, string machine, string instrument, float x, float y)
		{
			throw new NotImplementedException();
		}

		public void ConnectMachines(IMachine src, IMachine dst, int srcchn, int dstchn, int amp)
		{
			throw new NotImplementedException();
		}

		#endregion

		#region IActionStack Members

		public void BeginActionGroup()
		{
			throw new NotImplementedException();
		}

		public void EndActionGroup()
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}
