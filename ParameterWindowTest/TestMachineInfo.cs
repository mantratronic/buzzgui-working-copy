﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuzzGUI.Interfaces;
 

namespace ParameterWindowTest
{
	class TestMachineInfo : IMachineInfo
	{
		public MachineType Type	{ get { return MachineType.Effect; } }
		public int Version { get { return 45; } }
		public int InternalVersion { get { return 0; } }
		public MachineInfoFlags Flags { get { return MachineInfoFlags.STEREO_EFFECT; } }
		public int MinTracks { get { return 1; } }
		public int MaxTracks { get { return 32; } }
		public string Name { get { return "Jeskola Test Machine"; } }
		public string ShortName { get { return "TestMachine"; } }
		public string Author { get { return "Jesus"; } }

	}
}
