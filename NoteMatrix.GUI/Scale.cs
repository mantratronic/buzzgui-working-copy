﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoteMatrix
{
	public class Scale
	{
		public string Name { get; set; }
		public int[] Notes { get; set; }

		public Scale(string name, string notes)
		{
			Name = name;
			Notes = notes.Split(' ').Select(n => NoteToSemitones(n)).ToArray();
		}

		public static Scale[] Scales = 
		{
			new Scale("Chromatic", "C Db D Eb E F Gb G Ab A Bb B"),
			new Scale("Ionian", "C D E F G A B"),
			new Scale("Dorian", "C D Eb F G A Bb"),
			new Scale("Phrygian", "C Db Eb F G Ab Bb"),
			new Scale("Lydian", "C D E F# G A B"),
			new Scale("Mixolydian", "C D E F G A Bb"),
			new Scale("Aeolian", "C D Eb F G Ab Bb"),
			new Scale("Locrian", "C Db Eb F Ab Bb"),
			new Scale("Major Blues", "C Eb E G A Bb"),
			new Scale("Minor Blues", "C Eb F Gb G Bb"),
			new Scale("Diminish", "C D Eb F F# G# A B"),
			new Scale("Combination Diminish", "C Db Eb E F# G A Bb"),
			new Scale("Major Pentatonic", "C D E G A"),
			new Scale("Minor Pentatonic", "C Eb F G Bb"),
			new Scale("Ritusen Scale", "C D F G A"),
			new Scale("Raga Bhairav", "C Db E F G Ab B"),
			new Scale("Raga Gamanasrama", "C Db E F# G A B"),
			new Scale("Raga Todi", "C Db Eb F# G Ab B"),
			new Scale("Spanish Scale", "C Db Eb E F G Ab Bb"),
			new Scale("Gypsy Scale", "C D Eb F# G Ab B"),
			new Scale("Arabian Scale", "C D E F Gb Ab Bb"),
			new Scale("Egyptian Scale", "C D F G Bb"),
			new Scale("Hawaiian Scale", "C D Eb G A"),
			new Scale("Bali Island Pelog", "C Db Eb G Ab"),
			new Scale("Japanese Miyakobushi", "C Db F G Ab"),
			new Scale("Ryukyu Scale", "C E F G B"),
			new Scale("Wholetone", "C D E Gb Ab Bb"),
			new Scale("Minor 3rd Interval", "C Eb Gb A"),
			new Scale("3rd Interval", "C E Ab"),
			new Scale("4th Interval", "C F Bb"),
			new Scale("5th Interval", "C G"),
			new Scale("Octave Interval", "C")
		};

		static int NoteToSemitones(string note)
		{
			switch(note.ToLowerInvariant())
			{
				case "c": return 0;
				case "c#": case "db": return 1;
				case "d": return 2;
				case "d#": case "eb": return 3;
				case "e": return 4;
				case "f": return 5;
				case "f#":	case "gb": return 6;
				case "g": return 7;
				case "g#":	case "ab": return 8;
				case "a": return 9;
				case "a#": case "bb": return 10;
				case "b": return 11;
				default: throw new ArgumentException();
			}
		}
	}
}
