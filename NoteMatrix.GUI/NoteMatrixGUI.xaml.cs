﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;
using BuzzGUI.Common.InterfaceExtensions;

namespace NoteMatrix
{
	public class MachineGUIFactory : IMachineGUIFactory
	{
		public IMachineGUI CreateGUI(IMachineGUIHost host) { return new NoteMatrixGUI(); }
	}

	public partial class NoteMatrixGUI : UserControl, IMachineGUI, INotifyPropertyChanged
	{
		public ICommand CreatePatternCommand { get; private set; }
		public ICommand ClonePatternCommand { get; private set; }
		public ICommand DeletePatternCommand { get; private set; }

		public NoteMatrixGUI()
		{
			InitializeComponent();

			CreatePatternCommand = new SimpleCommand
			{
				CanExecuteDelegate = x => true,
				ExecuteDelegate = x =>
				{
					machine.CreatePattern(machine.GetNewPatternName(), 16);
				}
			};

			ClonePatternCommand = new SimpleCommand
			{
				CanExecuteDelegate = x => SelectedPattern != null,
				ExecuteDelegate = x =>
				{
					machine.ClonePattern(machine.GetNewPatternName(), SelectedPattern.Pattern);
				}
			};

			DeletePatternCommand = new SimpleCommand
			{
				CanExecuteDelegate = x => SelectedPattern != null,
				ExecuteDelegate = x =>
				{
					machine.DeletePattern(SelectedPattern.Pattern);
				}
			};

			new MachineGUIUpdateTimer(this, patternControl.TimerUpdate);

			DataContext = this;
		}

		IMachine machine;
		public IMachine Machine
		{
			get { return machine; }
			set
			{
				if (machine != null)
				{
					machine.PropertyChanged -= machine_PropertyChanged;
					machine.Graph.Buzz.Song.MachineAdded -= new Action<IMachine>(Song_MachineAdded);
					machine.Graph.Buzz.Song.MachineRemoved -= new Action<IMachine>(Song_MachineRemoved);
				}

				machine = value;

				if (machine != null)
				{
					machine.PropertyChanged += machine_PropertyChanged;
					machine.Graph.Buzz.Song.MachineAdded += new Action<IMachine>(Song_MachineAdded);
					machine.Graph.Buzz.Song.MachineRemoved += new Action<IMachine>(Song_MachineRemoved);

					UpdatePatternList();

					foreach (var m in machine.Graph.Buzz.Song.Machines)
						Song_MachineAdded(m);

					if (patterns != null && patterns.Count > 0)
						TargetMachine = machineList.Where(m => patterns[0].TargetMachine != null && m.Name == patterns[0].TargetMachine.Name).FirstOrDefault();

				}


			}
		}

		void machine_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "Patterns":
					UpdatePatternList();
					break;

				case "Name":
					IMachine m = sender as IMachine;
					if (m != machine)
					{
						var vm = machineList.First(x => x.Machine == m);
						PropertyChanged.Raise(vm, "Name");
					}
					break;
			}
		}

		void UpdatePatternList()
		{
			patterns = machine.Patterns.Select(p => new MatrixPattern(p)).ToList();
			
			foreach (var p in patterns)
			{
				if (p.TargetMachine == null && this.TargetMachine != null)
					p.TargetMachine = this.TargetMachine.Machine;
			}

			SelectedPattern = patterns.Count > 0 ? patterns[patterns.Count - 1] : null;

			PropertyChanged.Raise(this, "Patterns");
			PropertyChanged.Raise(this, "SelectedPattern");
		}

		public List<MatrixPattern> patterns;
		public IEnumerable<MatrixPattern> Patterns { get { return patterns; } }

		public MatrixPattern selectedPattern;
		public MatrixPattern SelectedPattern 
		{
			get { return selectedPattern; }
			set
			{
				if (selectedPattern != null)
				{
					selectedPattern.Pattern.PropertyChanged -= SelectedPattern_PropertyChanged;
				}

				selectedPattern = value;

				if (selectedPattern != null)
				{
					selectedPattern.Pattern.PropertyChanged += SelectedPattern_PropertyChanged;
				}

				PropertyChanged.Raise(this, "SelectedPattern");
				PropertyChanged.Raise(this, "IsPatternPlaying");
			}
		}

		void SelectedPattern_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "IsPlayingSolo":
					PropertyChanged.Raise(this, "IsPatternPlaying");
					break;
			}
			
		}

		public bool IsPatternPlaying
		{
			get { return selectedPattern != null ? selectedPattern.Pattern.IsPlayingSolo : false; }
			set { if (selectedPattern != null) selectedPattern.Pattern.IsPlayingSolo = value; }
		}


		void Song_MachineAdded(IMachine m)
		{
			if (m.DLL.Info.Type == MachineType.Master || m.Name == machine.Name) return;

			m.PropertyChanged += TargetMachinePropertyChanged;
			machineList.Add(new MachineVM(m));
			PropertyChanged.Raise(this, "MachineList");

		}

		void Song_MachineRemoved(IMachine m)
		{
			m.PropertyChanged -= TargetMachinePropertyChanged;
			var vm = machineList.FirstOrDefault(x => x.Machine == m);
			if (vm == null) return;
			machineList.Remove(vm);
			PropertyChanged.Raise(this, "MachineList");
		}

		void TargetMachinePropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "Name")
			{
				var vm = machineList.First(x => x.Machine == sender as IMachine);
				PropertyChanged.Raise(vm, "Name");
			}

		}

		public class MachineVM : INotifyPropertyChanged
		{
			public IMachine Machine { get; set; }
			public MachineVM(IMachine m) { Machine = m; }
			public string Name { get { return Machine.Name; } }

			#region INotifyPropertyChanged Members

			#pragma warning disable 67
			public event PropertyChangedEventHandler PropertyChanged;

			#endregion
		}

		List<MachineVM> machineList = new List<MachineVM>();
		public IEnumerable<MachineVM> MachineList { get { return machineList.OrderBy(m => m.Machine.Name); } }

		MachineVM targetMachine;
		public MachineVM TargetMachine
		{
			get
			{
				return targetMachine;
			}
			set
			{
				targetMachine = value;
				PropertyChanged.Raise(this, "TargetMachine");

				foreach (var p in patterns)
					p.TargetMachine = targetMachine != null ? targetMachine.Machine : null;
					
			}
		}


		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion

		/*
		#region IParameterInfo Members

		public IEnumerable<int> GetValidParameterValues(IParameter p, int track) { yield break; }
		public bool IsParameterVisible(IParameter p) { return false; }

		#endregion
		 */
	}
}
