﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Buzz.MachineInterface;

namespace NoteMatrix
{
	[MachineDecl(Name = "Jeskola Note Matrix", ShortName = "Note Matrix", Author = "Oskari Tammelin")]
	public class NoteMatrixMachine : IBuzzMachine
	{
		public NoteMatrixMachine(IBuzzMachineHost host)
		{
		}

		[ParameterDecl(MaxValue = 127, DefValue = 0)]
		public int Dummy { get; set; }
	}
}
