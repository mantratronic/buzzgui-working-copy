﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;

namespace NoteMatrix
{
	/// <summary>
	/// Interaction logic for PatternControl.xaml
	/// </summary>
	public partial class PatternControl : UserControl
	{
		/*

		Brush[] offBrushes = new[] { Brushes.DarkGreen, Brushes.DarkRed, Brushes.DarkMagenta, Brushes.DarkBlue, Brushes.DarkCyan };
		Brush[] onBrushes = new[] { Brushes.Green, Brushes.Red, Brushes.Magenta, Brushes.Blue, Brushes.Cyan };
		*/

		const double ElementSize = 22;

		Brush[] offBrushes = new[] { Brushes.DarkGoldenrod };
		Brush[] onBrushes = new[] { Brushes.Gold };

		MatrixPattern matrixPattern;


		void matrixPattern_ElementChanged(int row, int column, bool set)
		{
			var rect = grid.Children[row * grid.Columns + column] as Border;

			if (set)
				rect.Background = onBrushes[row % onBrushes.Length];
			else
				rect.Background = offBrushes[row % offBrushes.Length];
		}

		void matrixPattern_SizeChanged()
		{
			CreateGrid();
		}

		void CreateGrid()
		{
			grid.Children.Clear();
			if (matrixPattern == null) return;
			grid.Rows = matrixPattern.RowCount;
			grid.Columns = matrixPattern.ColumnCount;

			grid.MaxWidth = ElementSize * grid.Columns;

			bool drawState = true;

			for (int _r = 0; _r < grid.Rows; _r++)
			{
				int r = _r;

				for (int _c = 0; _c < grid.Columns; _c++)
				{
					int c = _c;

					var rect = new Border()
					{
						Background = offBrushes[r % offBrushes.Length],
						BorderBrush = Brushes.Black,
						BorderThickness = new Thickness(2.0),
						Margin = new Thickness(2),
						Width = ElementSize - 4,
						Height = ElementSize - 4
					};

					rect.MouseLeftButtonDown += (sender, e) =>
					{
						if (Keyboard.Modifiers == ModifierKeys.None)
							drawState = matrixPattern.ToggleElement(r, c);
					};

					rect.MouseLeftButtonUp += (sender, e) =>
					{
						drawState = true;
					};

					rect.MouseEnter += (sender, e) =>
					{
						if (e.LeftButton == MouseButtonState.Pressed && Keyboard.Modifiers == ModifierKeys.None)
						{
							matrixPattern.SetElement(r, c, drawState);
						}
					};

					grid.Children.Add(rect);
				}
			}

			matrixPattern.UpdateElements();
		}

		void SetColumnBorderBrush(int column, Brush br)
		{
			if (column < 0 || column >= grid.Columns) return;

			for (int r = 0; r < grid.Rows; r++)
			{
				var b = grid.Children[r * grid.Columns + column] as Border;
				b.BorderBrush = br;
			}
		}

		int lastPlayPosition = int.MinValue;

		public void TimerUpdate()
		{
			if (matrixPattern == null) return;

			int p = matrixPattern.PlayPosition;
			if (p == lastPlayPosition) return;

			if (lastPlayPosition != int.MinValue)
				SetColumnBorderBrush(lastPlayPosition, Brushes.Black);

			if (p != int.MinValue)
				SetColumnBorderBrush(p, Brushes.HotPink);

			lastPlayPosition = p;
		}

		public PatternControl()
		{
			InitializeComponent();

			this.IsEnabled = false;

			this.DataContextChanged += (sender, e) =>
			{
				if (matrixPattern != null)
				{
					matrixPattern.ElementChanged -= matrixPattern_ElementChanged;
					matrixPattern.SizeChanged -= matrixPattern_SizeChanged;
				}

				matrixPattern = e.NewValue as MatrixPattern;

				if (matrixPattern != null)
				{
					matrixPattern.ElementChanged += matrixPattern_ElementChanged;
					matrixPattern.SizeChanged += matrixPattern_SizeChanged;

				}

				this.IsEnabled = matrixPattern != null;
				CreateGrid();

			};

			this.PreviewKeyDown += (sender, e) =>
			{
				if (matrixPattern == null) return;

				if (e.Key == Key.Left) { matrixPattern.RotateElements(-1, 0); e.Handled = true; }
				if (e.Key == Key.Right) { matrixPattern.RotateElements(1, 0); e.Handled = true; }
				if (e.Key == Key.Up) { matrixPattern.RotateElements(0, -1); e.Handled = true; }
				if (e.Key == Key.Down) { matrixPattern.RotateElements(0, 1); e.Handled = true; }

			};

			var acc = new Vector();

			new Dragger
			{
				Element = this,
				Gesture = new DragMouseGesture() { Button = MouseButton.Middle },
				DragCursor = Cursors.ScrollAll,
				Mode = DraggerMode.Delta,
				BeginDrag = (p, alt, cc) => { acc.X = 0; acc.Y = 0; },
				Drag = d => 
				{
					acc += (Vector)d;

					int dx = (int)(acc.X / ElementSize);
					int dy = (int)(acc.Y / ElementSize);
					
					acc.X -= dx * ElementSize;
					acc.Y -= dy * ElementSize;

					if (dx != 0 || dy != 0) matrixPattern.RotateElements(dx, dy); 
				}
			};

		}

	}

}
