﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;
using BuzzGUI.Common.InterfaceExtensions;

namespace NoteMatrix
{
	public class MatrixPattern
	{
		IPatternColumn patternColumn;

		public IPattern Pattern { get; private set; }
		public string Name { get { return Pattern.Name; } }

		public int RowCount { get; private set; }
		public int ColumnCount { get { return Pattern.Length; } }

		public int PlayPosition { get { return Pattern.PlayPosition / PatternEvent.TimeBase; } }

		public IMachine TargetMachine 
		{
			get { return patternColumn.Machine; }
			set	{ patternColumn.Machine = value; }
		}

		public IEnumerable<Scale> Scales { get { return Scale.Scales; } }

		public Scale SelectedScale
		{
			get
			{
				string name;
				if (!patternColumn.Metadata.TryGetValue("NoteMatrixScale", out name))
					name = "Ritusen Scale";

				return Scales.First(s => s.Name == name);
			}
			set
			{
				patternColumn.Metadata["NoteMatrixScale"] = value.Name;
				ChangeScale();
			}
		}

		public IEnumerable<string> RootNotes { get { return BuzzNote.Names; } }

		public string SelectedRootNote
		{
			get
			{
				string name;
				if (!patternColumn.Metadata.TryGetValue("NoteMatrixRootNote", out name))
					name = "A-4";

				return RootNotes.First(n => n == name);
			}
			set
			{
				patternColumn.Metadata["NoteMatrixRootNote"] = value;
				ChangeScale();
			}

		}

		int noteDuration = PatternEvent.TimeBase / 2;
		int velocity = 100;
		int[] rowNotes;

		public MatrixPattern(IPattern pattern)
		{
			this.Pattern = pattern;

			RowCount = 16;

			rowNotes = new int[RowCount];

			patternColumn = Pattern.Columns.FirstOrDefault(c => c.Type == PatternColumnType.MIDI);
			if (patternColumn == null)
			{
				Pattern.InsertColumn(0, null);
				patternColumn = Pattern.Columns.First(c => c.Type == PatternColumnType.MIDI);
			}

			SetRowNotes();
		}

		void SetRowNotes()
		{
			var scale = SelectedScale.Notes;
			var rootNote = BuzzNote.ToMIDINote(BuzzNote.Parse(SelectedRootNote));

			for (int row = 0; row < RowCount; row++)
			{
				int i = RowCount - row - 1;
				rowNotes[row] = (rootNote + scale[i % scale.Length] + 12 * (i / scale.Length)) & 127;
			}
		}

		void patternColumn_EventsChanged(IEnumerable<PatternEvent> events, bool set)
		{
			UpdateElements();
		}

		void pattern_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "Length":
					if (SizeChanged != null) SizeChanged();
					break;

			}
		}

		IEnumerable<PatternEvent> PatternEvents { get {	return patternColumn.GetGridMIDINoteOnEvents(ColumnCount, PatternEvent.TimeBase, rowNotes);	} }
		IEnumerable<Tuple<int, int>> ActiveElements { get { return PatternEvents.Select(e => Tuple.Create(Array.IndexOf(rowNotes, MIDI.DecodeData1(e.Value)), e.Time / PatternEvent.TimeBase)); } }

		public void UpdateElements()
		{
			for (int r = 0; r < RowCount; r++)
			{
				for (int c = 0; c < ColumnCount; c++)
					RaiseElementChanged(r, c, false);
			}

			foreach (var e in ActiveElements) 
				RaiseElementChanged(e.Item1, e.Item2, true);

		}

		PatternEvent ElementToEvent(int row, int column)
		{
			return new PatternEvent(column * PatternEvent.TimeBase, MIDI.EncodeNoteOn(rowNotes[row], velocity), noteDuration);
		}

		public bool ToggleElement(int row, int column)
		{
			int t = column * PatternEvent.TimeBase;
			var newstate = !patternColumn.GetEvents(t, t + 1).Where(e => MIDI.IsNoteOn(e.Value, rowNotes[row])).Any();
			patternColumn.SetEvents(Enumerable.Repeat(ElementToEvent(row, column), 1), newstate);
			return newstate;
		}

		public void SetElement(int row, int column, bool newstate)
		{
			int t = column * PatternEvent.TimeBase;
			var oldstate = patternColumn.GetEvents(t, t + 1).Where(e => MIDI.IsNoteOn(e.Value, rowNotes[row])).Any();
			if (newstate != oldstate) patternColumn.SetEvents(Enumerable.Repeat(ElementToEvent(row, column), 1), newstate);
		}

		public void SetElements(IEnumerable<Tuple<int, int>> elements, bool newstate)
		{
			patternColumn.SetEvents(elements.Select(e => ElementToEvent(e.Item1, e.Item2)), newstate);
		}

		public void Clear()
		{
			patternColumn.SetEvents(PatternEvents, false);
		}

		public void RotateElements(int dcolumn, int drow)
		{
			var ae = ActiveElements.ToArray();
			Clear();

			SetElements(ae.Select(e => Tuple.Create(
				(int)((uint)(e.Item1 + drow) % RowCount),
				(int)((uint)(e.Item2 + dcolumn) % ColumnCount)
				)), true);
				
		}

		public void ChangeScale()
		{
			var ae = ActiveElements.ToArray();
			Clear();
			SetRowNotes();
			SetElements(ae, true);
		}


		event Action<int, int, bool> elementChanged;
		public event Action<int, int, bool> ElementChanged
		{
			add
			{
				if (elementChanged == null)
				{
					patternColumn.EventsChanged += patternColumn_EventsChanged;
					Pattern.PropertyChanged += pattern_PropertyChanged;
				}
				elementChanged += value;
			}
			remove
			{
				elementChanged -= value;
				if (elementChanged == null)
				{
					patternColumn.EventsChanged -= patternColumn_EventsChanged;
					Pattern.PropertyChanged -= pattern_PropertyChanged;
				}
			}
		}

		void RaiseElementChanged(int row, int column, bool set)
		{
			if (elementChanged != null) elementChanged(row, column, set);
		}

		public event Action SizeChanged;

	}
}
