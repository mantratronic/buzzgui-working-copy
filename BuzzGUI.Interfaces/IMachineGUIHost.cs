﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuzzGUI.Interfaces
{
	public interface IMachineGUIHost
	{
		void DoAction(IAction a);
	}
}
