﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace BuzzGUI.Interfaces
{
	[AttributeUsage(AttributeTargets.Class)]
	public class MachineGUIFactoryDecl : Attribute
	{
		public bool PreferWindowedGUI;
		public bool IsGUIResizable;
		public bool UseThemeStyles;
	}

	public interface IMachineGUIFactory
	{
		IMachineGUI CreateGUI(IMachineGUIHost host);
	}
}
