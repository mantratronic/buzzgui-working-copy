﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace BuzzGUI.Interfaces
{
	public interface IPattern : INotifyPropertyChanged
	{
		IMachine Machine { get; }
		string Name { get; }
		int Length { get; set; }										// length in buzz ticks

		ReadOnlyCollection<IPatternColumn> Columns { get; }

		int PlayPosition { get; }										// position in PatternEvent.TimeBase units or int.MinValue if not playing
		bool IsPlayingSolo { get; set; }

		int[] PatternEditorMachineMIDIEvents { get; }					// data from pxp or pianoroll as midi events (CMachineInterfaceEx::ExportMidiEvents)

		void InsertColumn(int index, IParameter p, int track);			// PatternColumnType.Parameter
		void InsertColumn(int index, IMachine m);						// PatternColumnType.MIDI

		void DeleteColumn(int index);

		void UpdatePEMachineWaveReferences(IDictionary<int, int> map);
		
		event Action<IPatternColumn> ColumnAdded;
		event Action<IPatternColumn> ColumnRemoved;
	}
}
