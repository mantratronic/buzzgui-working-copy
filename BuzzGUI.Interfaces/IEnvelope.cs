﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace BuzzGUI.Interfaces
{
	public interface IEnvelope : INotifyPropertyChanged
	{
		ReadOnlyCollection<Tuple<int, int>> Points { get; }
		int SustainPoint { get; }
		int PlayPosition { get; }
		bool IsEnabled { get; set; }

		void Update(IEnumerable<Tuple<int, int>> points, int sustainpoint);
	}
}
