﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuzzGUI.Interfaces
{
	public interface IIndex<K, V>
	{
		V this[K index] { get; }
	}
}
