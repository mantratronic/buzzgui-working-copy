﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Buzz.MachineInterface
{
	public interface IBuzzMachine
	{
		// Buzz uses reflection and delegates to access the machine's functions so there's only documentation here
		// {U} = ui thread, {A} = an audio thread, {M} = midi thread

		// Declaration:
		// [MachineDecl] public <any class name> : IBuzzMachine

		// Constructor:
		// {U} public <class name>(IBuzzMachineHost host);

		// Parameter properties:
		// {A} [ParameterDecl] public <bool | int | float | Interpolator> <parameter name> { get; set; }
		// Interpolator parameters must be new'd in the constructor and never changed

		// Generator Work functions:
		// {A} public Sample Work();
		// {A} public bool Work(Sample[] output, int n, WorkModes mode);

		// Effect Work functions:
		// {A} public Sample Work(Sample s);
		// {A} public bool Work(Sample[] output, Sample[] input, int n, WorkModes mode);

		// Machine state that is saved in files (CMachineInteface::Init, CMachineInteface::Load, CMachineInteface::Save):
		// {U} public <any class name> MachineState { get; set; }

		// Right-click menu commands:
		// {U} public IEnumerable<IMenuItem> Commands { get; }

		// Other functions:
		// {U} public void Stop();
		// {U} public void ImportFinished(IDictionary<string, string> machineNameMap);
		// {M} public void MidiNote(int const channel, int const value, int const velocity);
		// {M} public void MidiControlChange(int const ctrl, int const channel, int const value);

	}
}
