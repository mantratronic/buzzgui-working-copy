﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace BuzzGUI.Interfaces
{
	public interface IParameterMetadata : INotifyPropertyChanged
	{
		IEnumerable<int> GetValidParameterValues(int track);
		bool IsVisible { get; }
		double Indicator { get; }
	}
}
