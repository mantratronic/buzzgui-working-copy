﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows.Media;


namespace BuzzGUI.Interfaces
{
	public enum BuzzView { PatternView, MachineView, SequenceView, WaveTableView, SongInfoView };
	public enum BuzzCommand { NewFile, OpenFile, SaveFile, SaveFileAs, Cut, Copy, Paste, Undo, Redo, Stop, Exit, About, Preferences, DebugConsole };

	public interface IBuzz : INotifyPropertyChanged
	{
		ISong Song { get; }
		BuzzView ActiveView { get; set; }
		
		double MasterVolume { get; set; }
		Tuple<double, double> VUMeterLevel { get; }
		
		int BPM { get; set; }
		int TPB { get; set; }
		int Speed { get; set; }

		bool Playing { get; set; }
		bool Recording { get; set; }
		bool Looping { get; set; }
		bool AudioDeviceDisabled { get; set; }

		ReadOnlyCollection<string> MIDIControllers { get; }
		IIndex<string, Color> ThemeColors { get; }
		IMenuItem MachineIndex { get; }

		IMachine MIDIFocusMachine { get; set; }
		bool MIDIFocusLocked { get; set; }
		bool MIDIActivity { get; }

		bool IsPianoKeyboardVisible { get; set; }
		bool IsSettingsWindowVisible { get; set; }
		bool IsCPUMonitorWindowVisible { get; set; }
		bool IsHardDiskRecorderWindowVisible { get; set; }
		bool IsFullScreen { get; set; }

		int BuildNumber { get; }

		ReadOnlyDictionary<string, IMachineDLL> MachineDLLs { get; }
		ReadOnlyCollection<IInstrument> Instruments { get; }

		ReadOnlyCollection<string> AudioDrivers { get; }
		string SelectedAudioDriver { get; set; }
		int SelectedAudioDriverSampleRate { get; }

		bool OverrideAudioDriver { get; set; }

		IEditContext EditContext { get; set; }

		BuzzPerformanceData PerformanceData { get; }

		IntPtr MachineViewHWND { get; }
		int HostVersion { get; }											// MI_VERSION

		ReadOnlyCollection<string> Themes { get; }
		string SelectedTheme { get; set; }


		void ExecuteCommand(BuzzCommand cmd);
		bool CanExecuteCommand(BuzzCommand cmd);

		void DCWriteLine(string s);

		void ActivatePatternEditor();
		void ActivateSequenceEditor();
		void SetPatternEditorMachine(IMachine m);
		void SetPatternEditorPattern(IPattern p);
		void NewSequenceEditorActivated();

		void SendMIDIInput(int data);

		void ConfigureAudioDriver();

		int RenderAudio(float[] buffer, int nsamples, int samplerate);		// can be called when OverrideAudioDriver == true

		IMachine GetIMachineFromCMachinePtr(IntPtr pmac);

		void TakePerformanceDataSnapshot();									// updates IBuzz.PerformanceData and IMachine.PerformanceData

		void OpenSongFile(string filename);

		event Action<IOpenSong> OpenSong;
		event Action<ISaveSong> SaveSong;
		event Action<int> MIDIInput;
		event Action PatternEditorActivated;

		event Action<float[], bool, SongTime> MasterTap;			// fired in the GUI thread

	}

}
