﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace BuzzGUI.Interfaces
{
	public interface IAttribute
	{
		string Name { get; }
		int MinValue { get; }
		int MaxValue { get; }
		int DefValue { get; }

		int Value { get; set; }
	}
}
