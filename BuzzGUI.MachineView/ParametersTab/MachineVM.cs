﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.ComponentModel;
using System.Collections.ObjectModel;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;
using BuzzGUI.Common.InterfaceExtensions;

namespace BuzzGUI.MachineView.ParametersTab
{
	public class MachineVM : INotifyPropertyChanged
	{
		public IMachine Machine { get; private set; }
		public ParametersTabVM TabVM { get; private set; }

		public string Name { get { return Machine.Name; } }

		public Color TitleBackgroundColor { get { return Machine.GetThemeColor(); } }
		public Color TitleForegroundColor { get { return Machine.Graph.Buzz.ThemeColors["MV Machine Text"].EnsureContrast(TitleBackgroundColor); } }

		public ReadOnlyCollection<ParameterVM> Parameters { get; private set; }

		public Visibility Visibility 
		{ 
			get 
			{ 
				return (TabVM.MachineFilter.Length == 0 || Machine.Name.IndexOf(TabVM.MachineFilter, StringComparison.OrdinalIgnoreCase) >= 0)
					&& Parameters.Any(p => p.Visibility == Visibility.Visible)
					? Visibility.Visible : Visibility.Collapsed;
			}
		}

		public MachineVM(ParametersTabVM ptvm, IMachine machine)
		{
			TabVM = ptvm;
			Machine = machine;
			machine.PropertyChanged += machine_PropertyChanged;
			CreateParameters();
		}

		void machine_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "TrackCount":
					CreateParameters();
					break;

				case "Name":
					PropertyChanged.Raise(this, "Name");
					break;

			}
		}

		public void Release()
		{
			Machine.PropertyChanged -= machine_PropertyChanged;

			foreach (var p in Parameters)
				p.Release();
		}

		void CreateParameters()
		{
			if (Parameters != null)
			{
				foreach (var p in Parameters)
					p.Release();
			}

			Parameters = Machine.AllParametersAndTracks()
				.Where(pt => pt.Item1.Flags.HasFlag(ParameterFlags.State))
				.Select(pt => new ParameterVM(this, pt.Item1, pt.Item2)).ToReadOnlyCollection();

			PropertyChanged.Raise(this, "Parameters");
		}


		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion
	}
}
