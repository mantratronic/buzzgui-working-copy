﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.ComponentModel;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;
using BuzzGUI.Common.InterfaceExtensions;

namespace Qsamo.GUI
{
	public class Envelope : INotifyPropertyChanged
	{
		float[] values;
		public float[] Values { get { return values; } set { values = value; PropertyChanged.Raise(this, "Values"); } }
		public Color Color { get; set; }
		public string Name { get; set; }

		public Envelope()
		{
			
		}

		public void Reset()
		{
			for (int i = 0; i < values.Length; i++) values[i] = 0;
			PropertyChanged.Raise(this, "Values");
		}

		public void AddLine(Point a, Point b)
		{
			if (a.X > b.X)
			{
				AddLine(b, a);
				return;
			}

			for (var x = a.X; x <= b.X; x++)
			{
				int ix = (int)Math.Round(x);
				if (ix >= 0 && ix < values.Length) values[ix] = (float)(a.Y + (b.Y - a.Y) * (x - a.X) / (b.X - a.X + 1));
			}

			PropertyChanged.Raise(this, "Values");
		}

		public void Send(IMachine machine, int envindex)
		{
			if (machine == null) return;

			new MachineGUIMessage
			{
				Send = bw =>
				{
					bw.Write((int)QsamoGUI.Messages.SetCustomEnvelopeData);
					bw.Write(envindex);
					bw.Write(0);
					bw.Write(Values.Length);
					for (int i = 0; i < Values.Length; i++) bw.Write(Values[i]);

				},
				Machine = machine
			};
		}

		public void Receive(IMachine machine, int envindex)
		{
			if (machine == null) return;

			new MachineGUIMessage
			{
				Send = bw =>
				{
					bw.Write((int)QsamoGUI.Messages.GetCustomEnvelopeData);
					bw.Write(envindex);
				},
				Receive = br =>
				{
					int nvalues = br.ReadInt32();
					var values = new float[nvalues];

					for (int i = 0; i < nvalues; i++)
						values[i] = br.ReadSingle();

					Values = values;

				},
				Machine = machine
			};

		}

		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion
	}
}
