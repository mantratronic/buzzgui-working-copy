﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Documents;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;

namespace Qsamo.GUI
{
	class EnvelopeCanvas : FrameworkElement
	{
		VisualCollection visualChildren;

		const int TickCount = 16;
		const int TickWidth = 96;
		const int StepCount = 96;
		const int StepHeight = 16;

		int CenterY = (StepCount / 2) * StepHeight;

		const double MinValue = StepCount / -2;
		const double MaxValue = StepCount / 2;

		const double InactiveOpacity = 0.07;

		Envelope[] envelopes;
		public Envelope[] Envelopes
		{
			set
			{
				envelopes = value;
				CreateVisuals();
			}
		}
		List<EnvelopeVisual> visuals;

		int activeEnvelopeIndex;
		PlayPositionAdorner playPositionAdorner;

		public IMachineGUIHost Host { get; set; }

		public EnvelopeCanvas()
		{
			CreateVisuals();

			this.Loaded += (sender, e) =>
			{
				var al = AdornerLayer.GetAdornerLayer(this);
				if (al != null)
				{
					if (playPositionAdorner != null) al.Remove(playPositionAdorner);
					playPositionAdorner = new PlayPositionAdorner(this);
					al.Add(playPositionAdorner);
				}
			};

			Width = TickCount * TickWidth;
			Height = StepCount * StepHeight;

			Point lastPoint = new Point();
			float[] oldValues = null;

			new Dragger()
			{
				Gesture = new DragMouseGesture { Button = MouseButton.Left },
				AlternativeGesture = new DragMouseGesture { Button = MouseButton.Left, Modifiers = ModifierKeys.Control },
				Mode = DraggerMode.Absolute,
				BeginDrag = (p, alt, cc) =>
				{
					lastPoint = p;
					oldValues = envelopes[activeEnvelopeIndex].Values.ToArray();
				},
				Drag = p =>
				{
					bool snap = Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);

					envelopes[activeEnvelopeIndex].AddLine(DPToLP(lastPoint, snap), DPToLP(p, snap));
					lastPoint = p;
					this.BringIntoView(new Rect(p, new Size(1, 1)));
				},
				EndDrag = p =>
				{
					Host.DoAction(new SimpleAction<Envelope, float[]>()
					{
						DoDelegate = a =>
						{
							if (a.Target == null)
							{
								a.Target = envelopes[activeEnvelopeIndex];
								a.OldData = oldValues.ToArray();
								a.NewData = a.Target.Values.ToArray();
							}
							else
							{
								a.Target.Values = a.NewData.ToArray();
							}
						},
						UndoDelegate = a =>
						{
							a.Target.Values = a.OldData.ToArray();
						}
					});
				},
				Element = this,
			};

		}

		Point DPToLP(Point p, bool snap = false)
		{
			var y = (CenterY - p.Y) / StepHeight;
			if (snap) y = Math.Round(y);
			return new Point(p.X, Math.Min(Math.Max(y, MinValue), MaxValue));

		}

		void CreateVisuals()
		{
			visuals = new List<EnvelopeVisual>();
			visualChildren = new VisualCollection(this);
			visualChildren.Add(new EnvelopeBackgroundVisual(TickCount, TickWidth, StepCount, StepHeight));

			if (envelopes != null)
			{

				for (int i = 0; i < envelopes.Length; i++)
				{
					envelopes[i].Values = new float[TickCount * TickWidth];
					var ev = new EnvelopeVisual(CenterY, StepHeight) { Envelope = envelopes[i], Opacity = InactiveOpacity };
					visuals.Add(ev);
					visualChildren.Add(ev);
				}
			}

		}

		public void SetActiveEnvelope(int i)
		{
			visuals[activeEnvelopeIndex].Opacity = InactiveOpacity;
			visualChildren.Remove(visuals[i]);
			visualChildren.Add(visuals[i]);
			activeEnvelopeIndex = i;
			visuals[activeEnvelopeIndex].Opacity = 1.0;
		}

		public void Reset()
		{
			Host.DoAction(new SimpleAction<Envelope, float[]>()
			{
				DoDelegate = a =>
				{
					if (a.Target == null)
					{
						a.Target = envelopes[activeEnvelopeIndex];
						a.OldData = a.Target.Values.ToArray();
					}

					a.Target.Reset();
				},
				UndoDelegate = a =>
				{
					a.Target.Values = a.OldData.ToArray();
				}
			});
		}

		public void SetPlayPosition(int i, int pos)
		{
			if (i == activeEnvelopeIndex)
			{
				if (playPositionAdorner != null)
					playPositionAdorner.Position = pos;
			}
		}


		protected override int VisualChildrenCount { get { return visualChildren.Count; } }
		protected override Visual GetVisualChild(int index)
		{
			if (index < 0 || index >= visualChildren.Count) throw new ArgumentOutOfRangeException(); 
			return visualChildren[index];
		}

	}
}
