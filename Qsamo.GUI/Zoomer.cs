﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.ComponentModel;
using BuzzGUI.Common;

namespace Qsamo.GUI
{
    public class Zoomer : INotifyPropertyChanged
    {
		public int LevelCount { get; set; }
		public Action Reset { get; set; }
		public bool Ctrl { get; set; }

		int level;
		public double Level 
		{
			get { return level; }
			set
			{
				level = (int)Math.Round(value);

				DependencyObject o = element;
				do { o = VisualTreeHelper.GetParent(o); } while (!(o is ScrollViewer));
				var sv = o as ScrollViewer;

				var s = Scale;

//				Point cp = e.GetPosition(value);
//				Point p = e.GetPosition(sv);

				ScaleTransform st = new ScaleTransform(s, s);
				element.LayoutTransform = st;

				sv.ScrollToHorizontalOffset(element.ActualWidth / 2 * s - sv.ViewportWidth / 2);
				sv.ScrollToVerticalOffset(element.ActualHeight / 2 * s - sv.ViewportHeight / 2);

			}
		}

		double Scale
		{
			get
			{
				return Math.Pow(2.0, 3 * Level / (double)LevelCount);
			}
		}

		void SetLevel(int l, Point p, Point cp)
		{
			DependencyObject o = element;
			do { o = VisualTreeHelper.GetParent(o); } while (!(o is ScrollViewer));
			var sv = o as ScrollViewer;

			level = Math.Min(Math.Max(l, -LevelCount), LevelCount);

			var s = Scale;

			ScaleTransform st = new ScaleTransform(s, s);
			element.LayoutTransform = st;

			sv.ScrollToHorizontalOffset(cp.X * s - p.X);
			sv.ScrollToVerticalOffset(cp.Y * s - p.Y);

			if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Level"));

		}

		FrameworkElement element;
        public FrameworkElement Element
        {
            set
            {
				element = value;
                DependencyObject o = value;
                do { o = VisualTreeHelper.GetParent(o); } while (!(o is ScrollViewer));
                var sv = o as ScrollViewer;

                int acc = 0;

                value.PreviewMouseWheel += (sender, e) =>
                {
					if (!Ctrl || Keyboard.Modifiers == ModifierKeys.Control)
					{
						acc += e.Delta;
						int l = (int)Math.Round(Level);

						if (acc >= 120)
						{
							acc -= 120;
							l++;
						}
						else if (e.Delta <= -120)
						{
							acc += 120;
							l--;
						}

						Point cp = e.GetPosition(value);
						Point p = e.GetPosition(sv);

						SetLevel(l, p, cp);

						e.Handled = true;
					}

                };

				bool dragging = false;
				double zoomAcc = 0;
				double dragY = 0;

				value.MouseDown += (sender, e) =>
				{
					Point cp = e.GetPosition(value);
					Point p = e.GetPosition(sv);

					if (e.ChangedButton == MouseButton.Middle && e.ClickCount == 2)
					{
						level = 0;
						value.LayoutTransform = null;
//						sv.ScrollToHorizontalOffset(cp.X - p.X);
//						sv.ScrollToVerticalOffset(cp.Y - p.Y);
						if (Reset != null) Reset();
						if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Level"));
					}

					if (e.ChangedButton == MouseButton.Left && Keyboard.Modifiers == ModifierKeys.Alt)
					{
						dragY = p.Y;
						dragging = true;
						value.CaptureMouse();
					}
				};

				value.MouseUp += (sender, e) =>
				{
					if (dragging && e.ChangedButton == MouseButton.Left)
					{
						dragging = false;
						value.ReleaseMouseCapture();
					}
				};

				value.MouseMove += (sender, e) =>
				{
					Point cp = e.GetPosition(value);
					Point p = e.GetPosition(sv);

					if (dragging)
					{
						var delta = p.Y - dragY;
						dragY = p.Y;

						double res = 10;

						zoomAcc += delta;
						while (zoomAcc < res)
						{
							zoomAcc += res;
							SetLevel(level + 1, p, cp);
						}

						while (zoomAcc > res)
						{
							zoomAcc -= res;
							SetLevel(level - 1, p, cp);
						}
					}
				};



            }
        }


		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion
	}
}
