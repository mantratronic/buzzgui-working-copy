﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;
using BuzzGUI.Common.Presets;
using BuzzGUI.Common.InterfaceExtensions;

namespace Qsamo.GUI
{
	public class MachineGUIFactory : IMachineGUIFactory
	{
		public IMachineGUI CreateGUI(IMachineGUIHost host) { return new QsamoGUI(host); }
	}

	public partial class QsamoGUI : UserControl, IMachineGUI, IMachineMetadata, INotifyPropertyChanged
	{
		public enum Messages { GetInfo, SetCustomEnvelopeData, GetCustomEnvelopeData, SetModulatorData, GetModulatorData };
		public enum EnvelopeShapes { Lin, Quad, Exp };
		public enum AttackEnvelopeShapes { Lin, Quad, Exp, Cnvx, Cnvx2 };

		public IEnumerable<EnvelopeShapes> EnvelopeShapeList { get { return Enum.GetValues(typeof(EnvelopeShapes)).Cast<EnvelopeShapes>(); } }
		public IEnumerable<AttackEnvelopeShapes> AttackEnvelopeShapeList { get { return Enum.GetValues(typeof(AttackEnvelopeShapes)).Cast<AttackEnvelopeShapes>(); } }

		Envelope[] envelopes =
		{ 
			new Envelope() { Name = "Pitch", Color = Colors.Blue },
			new Envelope() { Name = "O2 Pitch", Color = Colors.Purple },
			new Envelope() { Name = "Level", Color = Colors.Brown },
			new Envelope() { Name = "O2 Level", Color = Colors.DarkGoldenrod },
			new Envelope() { Name = "Phase", Color = Colors.Green },
			new Envelope() { Name = "Cutoff", Color = Colors.Red }
		};

		Modulator[] modulators = new Modulator[Modulator.ModulatorCount];

		public ICommand RandomizeTimbreCommand { get; private set; }

		public QsamoGUI(IMachineGUIHost host)
		{
			InitializeComponent();

			envelopeControl.envelopeCanvas.Host = host;

			for (int i = 0; i < envelopes.Length; i++)
				InitSetCustomEnvelopeData(i);

			envelopeControl.Envelopes = envelopes;

			for (int i = 0; i < modulators.Length; i++)
			{
				modulators[i] = new Modulator();
				modulatorStack.Children.Add(new ModulatorEditor(modulators[i]));
				InitSetModulatorData(i);
			}

			DataContext = this;

			RandomizeTimbreCommand = new SimpleCommand()
			{
				CanExecuteDelegate = x => true,
				ExecuteDelegate = x => host.DoAction(new PresetAction(Machine, () => Randomizer.RandomizeTimbre(Machine)))
			};

			new MachineGUIUpdateTimer(this, () =>
			{
				new MachineGUIMessage
				{
					Send = bw => { bw.Write((int)Messages.GetInfo); },
					Receive = br =>
					{
						var activeVoiceCount = br.ReadInt32();
						var cutoff = br.ReadDouble();
						for (int i = 0; i < envelopes.Length; i++) envelopeControl.envelopeCanvas.SetPlayPosition(i, br.ReadInt32());

						statusText.Text = string.Format("Active Voices: {0}", activeVoiceCount);

						var cutoffmd = ParameterMetadata[Machine.GetParameter("Cutoff")] as ParameterMetadata;
						cutoffmd.Indicator = cutoff;
					},
					Machine = machine
				};

			});

		}

		void InitSetCustomEnvelopeData(int envindex)
		{
			var e = envelopes[envindex];
			e.PropertyChanged += (sender, _e) => { e.Send(machine, envindex); };
		}

		void InitSetModulatorData(int modindex)
		{
			var m = modulators[modindex];
			m.PropertyChanged += (sender, e) => { m.Send(machine, modindex); };
		}
	
		void GetMachineData()
		{
			for (int envindex = 0; envindex < envelopes.Length; envindex++)
			{
				var e = envelopes[envindex];
				e.Receive(machine, envindex);
			}

			for (int modindex = 0; modindex < modulators.Length; modindex++)
			{
				var m = modulators[modindex];
				m.Receive(machine, modindex);
			}

		}

		IMachine machine;
		public IMachine Machine
		{
			get { return machine; }
			set
			{
				if (machine != null)
				{
					machine.PropertyChanged -= new PropertyChangedEventHandler(machine_PropertyChanged);
				}

				machine = value;

				if (machine != null)
				{
					machine.PropertyChanged += new PropertyChangedEventHandler(machine_PropertyChanged);
					GetMachineData();
				}
			}
		}

		void machine_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "Data")
			{
				GetMachineData();
			}
			else if (e.PropertyName == "Attributes")
			{
				PropertyChanged.Raise(this, "Polyphony");
				PropertyChanged.Raise(this, "PitchWheelRangeUp");
				PropertyChanged.Raise(this, "PitchWheelRangeDown");
				PropertyChanged.Raise(this, "FilterDrive");
				PropertyChanged.Raise(this, "UnisonVoices");
				PropertyChanged.Raise(this, "UnisonSpread");
				PropertyChanged.Raise(this, "UnisonPanning");
				PropertyChanged.Raise(this, "UnisonOctaveMode");
				PropertyChanged.Raise(this, "LinearVelocity");
				PropertyChanged.Raise(this, "EnableFilter");
				PropertyChanged.Raise(this, "AmigaResamplerFilter");
				PropertyChanged.Raise(this, "DCBlocker");
				PropertyChanged.Raise(this, "FilterAfterAmp");
				PropertyChanged.Raise(this, "RandomPhase");
				PropertyChanged.Raise(this, "AttackShape");
				PropertyChanged.Raise(this, "DecayShape");
				PropertyChanged.Raise(this, "ReleaseShape");
				PropertyChanged.Raise(this, "OscillatorLeakage");
				PropertyChanged.Raise(this, "OscillatorDrift");
			}
		}

		#region Attributes

		public int Polyphony { get { return machine.GetAttribute("Polyphony").Value; } set { machine.GetAttribute("Polyphony").Value = value; } }
		public int PitchWheelRangeUp { get { return machine.GetAttribute("Pitch Wheel Range Up").Value; } set { machine.GetAttribute("Pitch Wheel Range Up").Value = value; } }
		public int PitchWheelRangeDown { get { return machine.GetAttribute("Pitch Wheel Range Down").Value; } set { machine.GetAttribute("Pitch Wheel Range Down").Value = value; } }
		public int FilterDrive { get { return machine.GetAttribute("Filter Drive (dB)").Value; } set { machine.GetAttribute("Filter Drive (dB)").Value = value; } }
		public int UnisonVoices { get { return machine.GetAttribute("Unison Voices").Value; } set { machine.GetAttribute("Unison Voices").Value = value; } }
		public int UnisonSpread { get { return machine.GetAttribute("Unison Spread (cents)").Value; } set { machine.GetAttribute("Unison Spread (cents)").Value = value; } }
		public int UnisonPanning { get { return machine.GetAttribute("Unison Panning Spread").Value; } set { machine.GetAttribute("Unison Panning Spread").Value = value; } }
		public int OscillatorLeakage { get { return machine.GetAttribute("Oscillator Leakage").Value; } set { machine.GetAttribute("Oscillator Leakage").Value = value; } }
		public int OscillatorDrift { get { return machine.GetAttribute("Oscillator Drift").Value; } set { machine.GetAttribute("Oscillator Drift").Value = value; } }
		public bool UnisonOctaveMode { get { return machine.GetAttribute("Unison Mode").Value != 0; } set { machine.GetAttribute("Unison Mode").Value = value ? 1 : 0; } }
		public bool LinearVelocity { get { return machine.GetAttribute("Linear Velocity").Value != 0; } set { machine.GetAttribute("Linear Velocity").Value = value ? 1 : 0; } }
		public bool EnableFilter { get { return machine.GetAttribute("Enable Filter").Value != 0; } set { machine.GetAttribute("Enable Filter").Value = value ? 1 : 0; } }
		public bool AmigaResamplerFilter { get { return machine.GetAttribute("Amiga Resampler Filter").Value != 0; } set { machine.GetAttribute("Amiga Resampler Filter").Value = value ? 1 : 0; } }
		public bool DCBlocker { get { return machine.GetAttribute("DC Blocker").Value != 0; } set { machine.GetAttribute("DC Blocker").Value = value ? 1 : 0; } }
		public bool FilterAfterAmp { get { return machine.GetAttribute("Filter After Amp").Value != 0; } set { machine.GetAttribute("Filter After Amp").Value = value ? 1 : 0; } }
		public bool RandomPhase { get { return machine.GetAttribute("Random Phase").Value != 0; } set { machine.GetAttribute("Random Phase").Value = value ? 1 : 0; } }
		public AttackEnvelopeShapes AttackShape { get { return (AttackEnvelopeShapes)machine.GetAttribute("Attack Shape").Value; } set { machine.GetAttribute("Attack Shape").Value = (int)value; } }
		public EnvelopeShapes DecayShape { get { return (EnvelopeShapes)machine.GetAttribute("Decay Shape").Value; } set { machine.GetAttribute("Decay Shape").Value = (int)value; } }
		public EnvelopeShapes ReleaseShape { get { return (EnvelopeShapes)machine.GetAttribute("Release Shape").Value; } set { machine.GetAttribute("Release Shape").Value = (int)value; } }

		#endregion

		#region IMachineMetadata Members


		Dictionary<IParameter, IParameterMetadata> parameterMetadata;
		public ReadOnlyDictionary<IParameter, IParameterMetadata> ParameterMetadata
		{
			get 
			{
				if (parameterMetadata == null)
					parameterMetadata = Machine.AllParameters().ToDictionary(p => p, p => (IParameterMetadata)new ParameterMetadata() { Parameter = p });

				return parameterMetadata.AsReadOnly();
			}
		}


		#endregion

		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion






	}
}
