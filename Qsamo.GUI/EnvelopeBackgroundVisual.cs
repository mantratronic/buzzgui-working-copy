﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Qsamo.GUI
{
	class EnvelopeBackgroundVisual : DrawingVisual
	{
		public EnvelopeBackgroundVisual(int nticks, int tickwidth, int nsteps, int stepheight)
		{
			int w = nticks * tickwidth;
			int h = nsteps * stepheight;

			var dc = RenderOpen();

			dc.DrawRectangle(Brushes.WhiteSmoke, null, new Rect(0, 0, w, h));

			for (int x = 0; x <= w; x += tickwidth)
			{
				dc.DrawRectangle(Brushes.DarkGray, null, new Rect(x, 0, 1.0, h));
			}

			for (int y = 0; y <= h; y += stepheight)
			{
				dc.DrawRectangle(y == nsteps / 2 * stepheight ? Brushes.Black : Brushes.DarkGray, null, new Rect(0, y, w, 1.0));
			}

			dc.Close();
		}


	}
}
