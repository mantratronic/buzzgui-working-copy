﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Documents;

namespace Qsamo.GUI
{
	class PlayPositionAdorner : Adorner
	{
		Brush brush;
		int position;

		public int Position
		{
			set
			{
				if (position != value)
				{
					position = value;
					InvalidateVisual();
				}
			}
		}

		public PlayPositionAdorner(UIElement adornedAElement)
			: base(adornedAElement)
		{
			IsHitTestVisible = false;
			brush = new SolidColorBrush(Color.FromArgb(150, 0, 0, 0));
			brush.Freeze();
		}

		protected override void OnRender(DrawingContext dc)
		{
			if (position >= 0 && position < AdornedElement.RenderSize.Width)
				dc.DrawRectangle(brush, null, new Rect(position, 0, 1, AdornedElement.RenderSize.Height));

		}

	}
}
