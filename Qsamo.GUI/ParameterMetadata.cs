﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;


namespace Qsamo.GUI
{
	public class ParameterMetadata : IParameterMetadata, INotifyPropertyChanged
	{
		static Random rnd = new Random();

		public IParameter Parameter { get; set; }

		public IEnumerable<int> GetValidParameterValues(int track)
		{
			if (Parameter.Name == "Waveform 1" || Parameter.Name == "Waveform 2" || Parameter.Name == "Waveform 3")
			{
				for (int i = 0; i < 4; i++) yield return i;
				yield return 4 + rnd.Next(4);		// return only one of the noise waveforms

				var wt = Parameter.Group.Machine.Graph.Buzz.Song.Wavetable;
				for (int i = 0; i < 32; i++)
				{
					if (wt.Waves[i] != null)
						yield return i + 8;
				}
			}
			else
			{
				yield break;
			}

		}

		public bool IsVisible
		{
			get { return true; }
		}

		double indicator = -1.0;
		public double Indicator
		{
			get { return indicator; }
			set
			{
				indicator = value;
				PropertyChanged.Raise(this, "Indicator");
			}
		}

		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion
	}
}
