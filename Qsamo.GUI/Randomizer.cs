﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;
using BuzzGUI.Common.InterfaceExtensions;

namespace Qsamo.GUI
{
	static class Randomizer
	{
		static Random rnd = new Random();

		static void PSwitch(params Tuple<double, Action>[] choices)
		{
			double r = rnd.NextDouble() * choices.Sum(c => c.Item1);
			choices.FindAndExecute(0.0, (a, c) => a + c.Item1, a => r < a, c => c.Item2());
		}

		static void Set(IMachine m, string name, int val)
		{
			m.GetParameter(name).SetValue(0, val);
		}

		static void Rnd(IMachine m, string name)
		{
			var param = m.GetParameter(name);
			param.SetValue(0, rnd.Next(param.MinValue, param.MaxValue + 1));
		}

		static void Rnd(IMachine m, string name, double prob, int def)
		{
			var param = m.GetParameter(name);

			if (rnd.NextDouble() < prob)
				param.SetValue(0, rnd.Next(param.MinValue, param.MaxValue + 1)); 
			else
				param.SetValue(0, def);
		}

		static void Rnd(IMachine m, string name, int minval, int maxval)
		{
			m.GetParameter(name).SetValue(0, rnd.Next(minval, maxval));
		}

		static void SetAttr(IMachine m, string name, int val)
		{
			m.GetAttribute(name).Value = val;
		}

		static void SetAttr(IMachine m, string name, double prob, int val, int def)
		{
			var a = m.GetAttribute(name);

			if (rnd.NextDouble() < prob)
				a.Value = val;
			else
				a.Value = def;
		}

		static void RndAttr(IMachine m, string name)
		{
			var a = m.GetAttribute(name);
			a.Value = rnd.Next(a.MinValue, a.MaxValue + 1);
		}

		static void RndAttr(IMachine m, string name, double prob, int def)
		{
			var a = m.GetAttribute(name);

			if (rnd.NextDouble() < prob)
				a.Value = rnd.Next(a.MinValue, a.MaxValue + 1);
			else
				a.Value = def;
		}

		public static void RandomizeTimbre(IMachine m)
		{
			Rnd(m, "Waveform 1", 0, 4);
			Rnd(m, "Waveform 2", 0, 4);
			Rnd(m, "Waveform 3", 0, 4);

			Rnd(m, "Combine");
			Rnd(m, "Combine 2", 0.75, 0);

			Rnd(m, "Phase");
			
			Level(m, "Level");
			Level(m, "W3 Level");

			Semitones(m, "Semitones");
			Semitones(m, "W3 Semitones");

			Finetune(m, "Finetune");
			Finetune(m, "W3 Finetune");

			Rnd(m, "Drive", 0.2, 64);
			Rnd(m, "Shape", 0.4, 0);

			RndAttr(m, "Oscillator Leakage", 0.5, 0);
			RndAttr(m, "Filter After Amp", 0.2, 0);

			RndAttr(m, "Unison Voices", 0.1, 1);
			RndAttr(m, "Unison Mode");
			RndAttr(m, "Unison Spread (cents)");
		}

		static void Level(IMachine m, string n)
		{
			PSwitch
			(
				Tuple.Create(5.0, new Action(() => Rnd(m, n, 64, 128))),
				Tuple.Create(3.0, new Action(() => Set(m, n, 127))),
				Tuple.Create(1.0, new Action(() => Rnd(m, n)))
			);

			
		}

		static void Semitones(IMachine m, string n)
		{
			PSwitch
			(
				Tuple.Create(10.0, new Action(() => Set(m, n, 64))),
				Tuple.Create(10.0, new Action(() => Set(m, n, 64 + 12))),
				Tuple.Create(10.0, new Action(() => Set(m, n, 64 - 12))),
				Tuple.Create(5.0, new Action(() => Set(m, n, 64 + 7))),
				Tuple.Create(3.0, new Action(() => Set(m, n, 64 + 24))),
				Tuple.Create(3.0, new Action(() => Set(m, n, 64 - 24))),
				Tuple.Create(1.0, new Action(() => Rnd(m, n)))
			);

		}

		static void Finetune(IMachine m, string n)
		{
			PSwitch
			(
				Tuple.Create(10.0, new Action(() => Set(m, n, 50))),
				Tuple.Create(10.0, new Action(() => Rnd(m, n, 50 - 5, 50 + 5 + 1))),
				Tuple.Create(1.0, new Action(() => Rnd(m, n)))
			);

		}

	}
}
