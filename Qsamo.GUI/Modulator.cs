﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;
using BuzzGUI.Common.InterfaceExtensions;

namespace Qsamo.GUI
{
	public class Modulator : INotifyPropertyChanged
	{
		public const int ModulatorCount = 8;

		public enum Sources { None, Key, Velocity, PitchWheel, ModWheel, Aftertouch, FootCtrl, ExpressionCtrl, BreathCtrl, RndUnipolar1, RndUnipolar2, RndBipolar1, RndBipolar2 };
		public enum Destinations { None, Phase, O2Level, Cutoff, Resonance, FilterEnvMod, FLFODepth, VibDepth, MEnvToPhase, MEnvToOsc2, MLFOToPhase, MLFOToOsc2, Finetune };

		Sources source = Sources.None;
		public Sources Source
		{
			get { return source; }
			set
			{
				source = value;
				PropertyChanged.Raise(this, "Source");
			}
		}

		Destinations destination = Destinations.None;
		public Destinations Destination
		{
			get { return destination; }
			set
			{
				destination = value;
				PropertyChanged.Raise(this, "Destination");
			}
		}

		int amount = 0;
		public int Amount
		{
			get { return amount; }
			set
			{
				amount = value;
				PropertyChanged.Raise(this, "Amount");
			}
		}

		public void Send(IMachine machine, int modindex)
		{
			if (machine == null) return;

			new MachineGUIMessage
			{
				Send = bw =>
				{
					bw.Write((int)QsamoGUI.Messages.SetModulatorData);
					bw.Write(modindex);
					bw.Write((int)Source);
					bw.Write((int)Destination);
					bw.Write(Amount);

				},
				Machine = machine
			};

		}

		public void Receive(IMachine machine, int modindex)
		{
			if (machine == null) return;

			new MachineGUIMessage
			{
				Send = bw =>
				{
					bw.Write((int)QsamoGUI.Messages.GetModulatorData);
					bw.Write(modindex);
				},
				Receive = br =>
				{
					Source = (Modulator.Sources)br.ReadInt32();
					Destination = (Modulator.Destinations)br.ReadInt32();
					Amount = br.ReadInt32();
				},
				Machine = machine
			};

		}


		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion
	}
}
