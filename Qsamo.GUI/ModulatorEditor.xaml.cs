﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using BuzzGUI.Common;

namespace Qsamo.GUI
{
	/// <summary>
	/// Interaction logic for ModulatorEditor.xaml
	/// </summary>
	public partial class ModulatorEditor : UserControl, INotifyPropertyChanged
	{
		public Modulator Modulator { get; private set; }
				
		public ModulatorEditor(Modulator m)
		{
			Modulator = m;
			DataContext = m;
			InitializeComponent();

			foreach (var s in Enum.GetValues(typeof(Modulator.Sources))) source.Items.Add(s);
			foreach (var s in Enum.GetValues(typeof(Modulator.Destinations))) destination.Items.Add(s);
		}

		#region INotifyPropertyChanged Members

		#pragma warning disable 67
		public event PropertyChangedEventHandler PropertyChanged;

		#endregion
	}
}
