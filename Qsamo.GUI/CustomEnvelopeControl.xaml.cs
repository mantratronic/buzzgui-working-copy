﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BuzzGUI.Common;

namespace Qsamo.GUI
{
	/// <summary>
	/// Interaction logic for CustomEnvelopeControl.xaml
	/// </summary>
	public partial class CustomEnvelopeControl : UserControl
	{
		Envelope[] envelopes;

		public Envelope[] Envelopes
		{
			set
			{
				envelopes = value;
				envelopeCanvas.Envelopes = value;

				radioButtonStack.Children.Clear();
				
				for (int _i = 0; _i < envelopes.Length; _i++)
				{
					var i = _i;

					var rb = new RadioButton()
					{
						Content = envelopes[i].Name,
						Foreground = new SolidColorBrush(envelopes[i].Color),
						VerticalAlignment = System.Windows.VerticalAlignment.Center,
						Margin = new Thickness(0, 0, 8, 0),
					};

					rb.Checked += (sender, e) => { envelopeCanvas.SetActiveEnvelope(i); };

					radioButtonStack.Children.Add(rb);

				}

				(radioButtonStack.Children[0] as RadioButton).IsChecked = true;
			}
		}

		bool loaded = false;
		bool layoutUpdated = false;

		public SimpleCommand ResetCommand { get; private set; }

		public CustomEnvelopeControl()
		{
			InitializeComponent();

			DataContext = this;

			ResetCommand = new SimpleCommand
			{
				CanExecuteDelegate = x => true,
				ExecuteDelegate = format =>
				{
					envelopeCanvas.Reset();
				}
			};

			this.LayoutUpdated += (sender, e) =>
			{
				if (!layoutUpdated && IsVisible)
				{
					layoutUpdated = true;
					sv.ScrollToVerticalOffset(envelopeCanvas.Height / 2 - sv.ViewportHeight / 2);
				}
			};

			this.Loaded += (sender, e) =>
			{
				if (!loaded && IsVisible)
				{
					loaded = true;

					var zoomer = new Zoomer
					{
						Element = envelopeCanvas,
						LevelCount = 8,
						Ctrl = false,
						Reset = () =>
						{
							sv.ScrollToHorizontalOffset(0);
							sv.ScrollToVerticalOffset(envelopeCanvas.Height / 2 - sv.ViewportHeight / 2);
						}
					};

					new Dragger
					{
						Element = this,
						Gesture = new DragMouseGesture { Button = MouseButton.Middle },
						AlternativeGesture = new DragMouseGesture { Button = MouseButton.Left, Modifiers = ModifierKeys.Shift },
						DragCursor = Cursors.ScrollAll,
						Drag = delta =>
						{
							sv.ScrollToHorizontalOffset(sv.HorizontalOffset - delta.X);
							sv.ScrollToVerticalOffset(sv.VerticalOffset - delta.Y);
						}
					};
				}

			};

		}
	}
}
