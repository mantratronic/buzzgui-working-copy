﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Qsamo.GUI
{
	class EnvelopeVisual : DrawingVisual
	{
		Envelope envelope;
		public Envelope Envelope
		{
			get { return envelope; }
			set
			{
				if (envelope != null)
				{
					envelope.PropertyChanged -= EnvelopePropertyChanged;
				}

				envelope = value;

				if (envelope != null)
				{
					envelope.PropertyChanged += EnvelopePropertyChanged;

					var br = new SolidColorBrush(envelope.Color);
					br.Freeze();
					pen = new Pen(br, 2);
					pen.Freeze();
				}

				Invalidate();
			}
		}

		void EnvelopePropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			Invalidate();
		}

		double centerY;
		double stepHeight;
		Pen pen;

		public EnvelopeVisual(int centery, double stepheight)
		{
			this.centerY = centery;
			this.stepHeight = stepheight;
		}

		public void Invalidate()
		{
			var pf = new PathFigure();
			double lasty = 0;
			int lastx = 0;

			for (int x = 0; x < envelope.Values.Length; x++)
			{
				var y = centerY - envelope.Values[x] * stepHeight;

				if (x == 0)
				{
					pf.StartPoint = new Point(x, y);
				}
				else
				{
					if (y != lasty || x == envelope.Values.Length - 1)
					{
						if (x - lastx > 1)
							pf.Segments.Add(new LineSegment(new Point(x - 1, lasty), true) { IsSmoothJoin = true });

						pf.Segments.Add(new LineSegment(new Point(x, y), true) { IsSmoothJoin = true });
						lastx = x;
					}
				}

				lasty = y;
			}

			pf.Freeze();

			var pg = new PathGeometry(new[] { pf });
			pg.Freeze();

			var dc = RenderOpen();
			dc.DrawGeometry(null, pen, pg);
			dc.Close();
		}


	}
}
