﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;
using BuzzGUI.Common.Presets;

namespace BuzzGUI.ParameterWindow.Actions
{
	class SelectPresetAction : IAction
	{
		ParameterWindowVM vm;
		Preset preset;
		Preset oldpreset;

		public SelectPresetAction(ParameterWindowVM vm, Preset p)
		{
			this.vm = vm;
			preset = p;
		}

		public void Do()
		{
			oldpreset = new Preset(vm.Machine, false, true);
			preset.Apply(vm.Machine, true);
		}

		public void Undo()
		{
			oldpreset.Apply(vm.Machine, true);
		}

	}
}
