﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;

namespace BuzzGUI.ParameterWindow
{
	public class EditContext : IEditContext
	{
		ParameterWindowVM window;
		ManagedActionStack actionStack = new ManagedActionStack();
		public ManagedActionStack ManagedActionStack { get { return actionStack; } }

		public EditContext(ParameterWindowVM window)
		{
			this.window = window;
		}

		public IActionStack ActionStack { get { return actionStack; } }
		public ICommand CutCommand { get { return null; } }
		public ICommand CopyCommand	{ get { return window.CopyPresetCommand; } }
		public ICommand PasteCommand { get { return window.PastePresetCommand; } }

	}
}
