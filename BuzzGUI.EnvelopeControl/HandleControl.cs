﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BuzzGUI.Common;

namespace BuzzGUI.EnvelopeControl
{
	public class HandleControl : Control
	{
		static HandleControl()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(HandleControl), new FrameworkPropertyMetadata(typeof(HandleControl)));
		}

		EnvelopeControl ec;
		int index;

		public ICommand DeleteCommand { get; private set; }
		public ICommand SustainCommand { get; private set; }

		public HandleControl(EnvelopeControl ec, int index)
		{
			this.ec = ec;
			this.index = index;
			DataContext = this;

			DeleteCommand = new SimpleCommand
			{
		        CanExecuteDelegate = x => true,
		        ExecuteDelegate = x => { ec.DeletePoint(index); }
			};

			SustainCommand = new SimpleCommand
			{
				CanExecuteDelegate = x => true,
				ExecuteDelegate = x => { ec.SustainPoint(index); }
			};

		}



	}
}
