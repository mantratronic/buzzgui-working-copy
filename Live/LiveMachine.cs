﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.ComponentModel;
using System.Windows.Input;
using Buzz.MachineInterface;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;
using System.Windows.Media;

namespace Jeskola.Live
{
	[MachineDecl(Name = "Jeskola Live", ShortName = "Live", Author = "Oskari Tammelin")]
	public class LiveMachine : IBuzzMachine
	{
		IBuzzMachineHost host;

		public LiveMachine(IBuzzMachineHost host)
		{
			this.host = host;
		}

		[ParameterDecl(MinValue = 1, MaxValue = 128, DefValue = 16)]
		public int Bar { get; set; }

	}

}
