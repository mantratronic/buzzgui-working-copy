﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jeskola.Live
{
	public interface ITrackVM
	{
		void Release();
		void Update();
	}
}
