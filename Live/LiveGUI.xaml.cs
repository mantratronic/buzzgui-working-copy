﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;

namespace Jeskola.Live
{
	[MachineGUIFactoryDecl(PreferWindowedGUI = true, IsGUIResizable = true)]
	public class MachineGUIFactory : IMachineGUIFactory 
	{ 
		public IMachineGUI CreateGUI(IMachineGUIHost host) { return new LiveGUI(); }

		/*
		Style windowStyle;
		public Style WindowStyle
		{
			get
			{
				if (windowStyle == null)
				{
					var rd = Application.LoadComponent(new Uri("/Jeskola Live.NET;component/WindowStyle.xaml", UriKind.RelativeOrAbsolute)) as ResourceDictionary;
					windowStyle = rd["LiveWindowStyle"] as Style;
				}
				return windowStyle;
			}
		}
		*/
	}

	/// <summary>
	/// Interaction logic for LiveGUI.xaml
	/// </summary>
	public partial class LiveGUI : UserControl, IMachineGUI
	{
		IMachine machine;
		LiveMachine liveMachine;
		LiveVM viewModel;

		public IMachine Machine
		{
			get { return machine; }
			set
			{
				if (machine != null)
				{
					viewModel.Release();
					this.DataContext = viewModel = null;
				}

				machine = value;

				if (machine != null)
				{
					liveMachine = (LiveMachine)machine.ManagedMachine;
					this.DataContext = viewModel = new LiveVM(machine);
				}
			}
		}

		public LiveGUI()
		{
			InitializeComponent();

			new MachineGUIUpdateTimer(this, () =>
			{
				if (viewModel != null)
					viewModel.Update();
			});

		}
	}
}
