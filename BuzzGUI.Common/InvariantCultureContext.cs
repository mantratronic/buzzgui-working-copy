﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Globalization;

namespace BuzzGUI.Common
{
	public class InvariantCultureContext : IDisposable
	{
		CultureInfo oldCulture;

		public InvariantCultureContext()
		{
			oldCulture = Thread.CurrentThread.CurrentCulture;
			Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
		}

		public void Dispose()
		{
			Thread.CurrentThread.CurrentCulture = oldCulture;
		}
	}
}
