﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuzzGUI.Interfaces;

namespace BuzzGUI.Common
{
	public class ActionGroup : IDisposable
	{
		IActionStack actionStack;

		public ActionGroup(IActionStack a)
		{
			actionStack = a;
			actionStack.BeginActionGroup();
		}

		public void Dispose()
		{
			actionStack.EndActionGroup();
		}
	}
}
