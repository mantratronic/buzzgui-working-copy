﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuzzGUI.Common.Settings
{
	public class GeneralSettings : Settings
	{
		[BuzzSetting(true, Description="Rename previous version to <songname>.backup when saving a song.")]
		public bool AutomaticBackups { get; set; }

		[BuzzSetting(true, Description="Check jeskola.net for updates at startup.")]
		public bool CheckForUpdates { get; set; }

		[BuzzSetting(false, Description = "Disable hardware accelerated (GPU) graphics rendering. Not recommended unless your GPU/drivers suck. Restart required.")]
		public bool WPFSoftwareRendering { get; set; }

	}
}
