﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;
using BuzzGUI.Common.Presets;

namespace BuzzGUI.Common
{
	public class PresetAction : SimpleAction<Preset>
	{
		public PresetAction(IMachine m, Action action)
		{
			DoDelegate = a =>
			{
				if (a.OldData == null) 
					a.OldData = new Preset(m, false, true);

				if (a.NewData == null)
				{
					action();
					a.NewData = new Preset(m, false, true);
				}
				else
				{
					a.NewData.Apply(m, true);
				}
			};
			UndoDelegate = a =>
			{
				a.OldData.Apply(m, true);
			};
		}
	}
}
