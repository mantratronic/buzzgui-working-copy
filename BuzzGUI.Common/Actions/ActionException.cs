﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuzzGUI.Interfaces;

namespace BuzzGUI.Common.Actions
{
	public class ActionException : Exception
	{
		IAction action;

		public ActionException(IAction action)
		{
			this.action = action;
		}

		public override string ToString()
		{
			return "ActionException: " + action.ToString();
		}
	}
}
