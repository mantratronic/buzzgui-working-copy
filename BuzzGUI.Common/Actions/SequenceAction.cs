﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;

namespace BuzzGUI.Common.Actions
{
	public abstract class SequenceAction : SongAction
	{
		int seqIndex;
		protected ISequence Sequence
		{
			get
			{
				if (seqIndex >= Song.Sequences.Count)
					throw new ActionException(this);
				return Song.Sequences[seqIndex];
			}
		}

		public SequenceAction(ISequence sequence)
			: base(sequence.Machine.Graph.Buzz.Song)
		{
			seqIndex = Song.Sequences.IndexOf(sequence);
		}

	}
}
