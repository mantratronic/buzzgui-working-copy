﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;

namespace BuzzGUI.Common.Actions
{
	public abstract class MachineAction : BuzzAction
	{
		ISong song;
		string machineName;

		protected IMachine Machine
		{
			get
			{
				var m = song.Machines.FirstOrDefault(x => x.Name == machineName);
				if (m == null) throw new ActionException(this);
				return m;
			}
		}

		public MachineAction(IMachine machine)
		{
			song = machine.Graph.Buzz.Song;
			machineName = machine.Name;
		}

	}
}
