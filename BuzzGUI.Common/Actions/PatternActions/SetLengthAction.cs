﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;
using BuzzGUI.Common.Actions;

namespace BuzzGUI.Common.Actions.PatternActions
{
	public class SetLengthAction : PatternAction
	{
		int newLength;
		int oldLength;

		public SetLengthAction(IPattern pattern, int length)
			: base(pattern)
		{
			this.newLength = length;
			this.oldLength = pattern.Length;
		}

		protected override void DoAction()
		{
			Pattern.Length = newLength;
		}

		protected override void UndoAction()
		{
			Pattern.Length = oldLength;
		}

	}
}
