﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;
using BuzzGUI.Common.Actions;

namespace BuzzGUI.Common.Actions.PatternActions
{
	public class SetBeatSubdivisionAction : PatternAction
	{
		Tuple<PatternColumnBeatRef, int>[] newSubdiv;
		int[] oldSubdiv;

		public SetBeatSubdivisionAction(IPattern pattern, IEnumerable<Tuple<PatternColumnBeatRef, int>> subdiv)
			: base(pattern)
		{
			this.newSubdiv = subdiv.ToArray();
			oldSubdiv = newSubdiv
				.Select(s => s.Item1.GetColumn(pattern).BeatSubdivision.ContainsKey(s.Item1.Beat) 
				? s.Item1.GetColumn(pattern).BeatSubdivision[s.Item1.Beat] : -1).ToArray();
		}

		protected override void DoAction()
		{
			var p = Pattern;
			newSubdiv.Run(s => s.Item1.GetColumn(p).SetBeatSubdivision(s.Item1.Beat, s.Item2));
		}

		protected override void UndoAction()
		{
			var p = Pattern;
			newSubdiv.Run((s, i) => { if (oldSubdiv[i] >= 0) s.Item1.GetColumn(p).SetBeatSubdivision(s.Item1.Beat, oldSubdiv[i]); } );
		}

	}
}
