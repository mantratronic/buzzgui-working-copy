﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;
using BuzzGUI.Common.Actions;

namespace BuzzGUI.Common.Actions
{
	public abstract class PatternAction : MachineAction
	{
		string patternName;

		protected IPattern Pattern
		{
			get
			{
				var p = Machine.Patterns.FirstOrDefault(x => x.Name == patternName);
				if (p == null) throw new ActionException(this);
				return p;
			}
		}

		public PatternAction(IPattern p)
			: base(p.Machine)
		{
			patternName = p.Name;
		}


	}
}
