﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuzzGUI.Interfaces;
using BuzzGUI.Common;

namespace BuzzGUI.Common.Actions
{
	public abstract class SongAction : BuzzAction
	{
		protected ISong Song { get; private set; }

		public SongAction(ISong song)
		{
			this.Song = song;
		}

	}
}
