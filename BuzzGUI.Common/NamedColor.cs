﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace BuzzGUI.Common
{
	public class NamedColor
	{
		public string Name { get; set; }
		public Color Color { get; set; }
	}
}
