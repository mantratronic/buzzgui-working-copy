﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuzzGUI.Interfaces;
using System.Xml.Serialization;
using BuzzGUI.Common;

namespace BuzzGUI.Common.Templates
{
	public class Markers
	{
		[XmlAttribute]
		public int LoopStart;

		[XmlAttribute]
		public int LoopEnd;

		[XmlAttribute]
		public int SongEnd;

		public Markers() { }
		public Markers(ISong song)
		{
			LoopStart = song.LoopStart;
			LoopEnd = song.LoopEnd;
			SongEnd = song.SongEnd;
		}
	}
}
