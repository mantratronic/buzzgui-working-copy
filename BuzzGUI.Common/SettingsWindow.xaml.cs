﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Reflection;
using System.Windows.Interop;
using BuzzGUI.Common.Settings;

namespace BuzzGUI.Common
{
	/// <summary>
	/// Interaction logic for SettingsWindow.xaml
	/// </summary>
	public partial class SettingsWindow : Window
	{
		static SortedDictionary<string, BuzzGUI.Common.Settings.Settings> settingsDictionary = new SortedDictionary<string, Settings.Settings>();

		public SettingsWindow()
		{
			InitializeComponent();
			this.Closed += (sender, e) =>
			{
				foreach (var s in settingsDictionary)
					s.Value.Save();
			};

			this.KeyDown += (sender, e) =>
			{
				if (e.Key == Key.Escape)
				{
					e.Handled = true;
					Close();
				}
			};

			foreach (var s in settingsDictionary)
				AddTab(s.Key, s.Value);
		}

		static SettingsWindow openWindow = null;

		public static bool IsWindowVisible { get { return openWindow != null; } }
		public static void CloseWindow() { if (IsWindowVisible) openWindow.Close(); }

		public static void Show(FrameworkElement v, string activetab)
		{
			if (openWindow != null)
			{
				ActivateTab(activetab);
				openWindow.Show();
				openWindow.Activate();
				return;
			}

			SettingsWindow sw = new SettingsWindow()
			{
				WindowStartupLocation = WindowStartupLocation.CenterOwner,
				Resources = v != null ? v.Resources : null,
			};
			sw.Closed += (sender, e) =>
			{
				openWindow = null;
			};

			new WindowInteropHelper(sw).Owner = v != null ? ((HwndSource)PresentationSource.FromVisual(v)).Handle : BuzzGUI.Common.Global.MachineViewHwndSource.Handle;

			openWindow = sw;
			ActivateTab(activetab);
			sw.Show();

		}

		static void ActivateTab(string header)
		{
			var item = openWindow.tc.Items.Cast<TabItem>().FirstOrDefault(i => (string)i.Header == header);
			if (item != null) openWindow.tc.SelectedItem = item;
		}

		public static void AddSettings(string header, BuzzGUI.Common.Settings.Settings settings)
		{
			settingsDictionary[header] = settings;
		}

		public static IEnumerable<Dictionary<string, string>> GetSettings()
		{
			return settingsDictionary.Select(sd => sd.Value.List.ToDictionary(s => sd.Key + "/" + s.Name, s => s.Value));
		}

		void AddTab(string header, BuzzGUI.Common.Settings.Settings settings)
		{
			var grid = new Grid() { Margin = new Thickness(8) };
			grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1.0, GridUnitType.Star) });
			grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1.0, GridUnitType.Star) });

			int row = 0;

			foreach (var _p in settings.List)
			{
				var p = _p;
				grid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });

				var tb = new TextBlock() { Text = p.Name, VerticalAlignment = VerticalAlignment.Center, Margin = new Thickness(0, 0, 4, 4), ToolTip = p.Description };
				if (p.Presets != null) tb.FontWeight = FontWeights.Bold;
				
				Grid.SetRow(tb, row);
				grid.Children.Add(tb);

				var cb = new ComboBox() { Margin = new Thickness(0, 0, 0, 4), ItemTemplate = Resources["ComboBoxItemTemplate"] as DataTemplate };
				Grid.SetRow(cb, row);
				Grid.SetColumn(cb, 1);
				grid.Children.Add(cb);
		
				cb.ItemsSource = p.Options;
				cb.SetBinding(ComboBox.SelectedItemProperty, new Binding("Value") { Source = p, Mode = BindingMode.TwoWay });

				// TODO: default value in bold

				row++;
			}

			var sv = new ScrollViewer() { Content = grid, VerticalScrollBarVisibility = ScrollBarVisibility.Auto, HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden };
			var ti = new TabItem() { Header = header, Content = sv, Tag = settings };
			tc.Items.Add(ti);
			
		}



	}
}
